package sample;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import sample.taskwork.Contact;
import sample.taskwork.IContact;
import sample.taskwork.TaskWork;

public class SeeController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TableView tableContact;

    @FXML
    private TableColumn<Contact, String> nameColum;

    @FXML
    private TableColumn<Contact, String> numbColum;


    private ObservableList<IContact> contactList= FXCollections.observableArrayList();

    @FXML
    void initialize() {
        update();
    }
    public void update(){

        tableContact.getItems().removeAll(contactList);
        TaskWork.getTableColum2(contactList);

        tableContact.setItems(contactList);
        nameColum.setCellValueFactory(cellData -> new SimpleObjectProperty<>( cellData.getValue().getName()));
        numbColum.setCellValueFactory(cellData -> new SimpleObjectProperty<>( cellData.getValue().getNumber()));

    }
}
