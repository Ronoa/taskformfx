package sample;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import sample.client.ClientLoader;
import sample.client.PacketPeresilka;
import sample.client.PacketRemoveTask;
import sample.taskwork.ITask;
import sample.taskwork.TaskWork;

public class InfoController {

    private ITask tempTask;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXTextArea textAreaDescription;

    @FXML
    private JFXTextField textAreaName;

    @FXML
    private JFXButton buttonFiveMin;

    @FXML
    private JFXButton buttonTenMin;

    @FXML
    private JFXButton buttonEnd;

    @FXML
    private Label labelInformation;
boolean del=false;
boolean check=false;

    Thread trio=new Thread() {
        @Override
        public void run() {
            try {
                Thread.sleep(20000);
                if(check==false){
                    tempTask.setOtlozhit(tempTask.getOtlozhit() + 1);
                    tempTask.setYvedomlenie(true);
                    //send
                    ClientLoader.sendPacket(new PacketPeresilka(tempTask));
                    del=true;
//                    try {
//                        Stage st=(Stage)  buttonFiveMin.getScene().getWindow();
//                        st.close();
//                    }
//                    catch (Exception e) {
//                        e.printStackTrace();
//                    }
                    Thread.currentThread().interrupt();
                }else {
                    Thread.currentThread().interrupt();
                }

            } catch (InterruptedException e) {

            }
        }
    };

    @FXML
    void initialize() {
        buttonFiveMin.setOnAction(event -> {
            //+5минут
            tempTask.setOtlozhit(tempTask.getOtlozhit() + 1);
            tempTask.setYvedomlenie(true);
            //
            ClientLoader.sendPacket(new PacketPeresilka(tempTask));


            Thread.currentThread().interrupt();
            buttonTenMin.getScene().getWindow().hide();
check=true;
            trio.interrupt();

        });
        buttonTenMin.setOnAction(event -> {
            //+10минут
            tempTask.setOtlozhit(tempTask.getOtlozhit() + 2);
            tempTask.setYvedomlenie(true);
            //send
            ClientLoader.sendPacket(new PacketPeresilka(tempTask));

            buttonTenMin.getScene().getWindow().hide();
            check=true;
            trio.interrupt();

        });
        buttonEnd.setOnAction(event -> {
            //end
            ClientLoader.sendPacket(new PacketRemoveTask(tempTask));
            TaskWork.removeTask2(tempTask);


            Platform.runLater(() -> {

                String str = tempTask.getName();
                TaskWork.trayIcon.displayMessage(
                        "Выполненная задача:",
                        str,
                        java.awt.TrayIcon.MessageType.INFO
                );
            });
            buttonEnd.getScene().getWindow().hide();
            check=true;
            trio.interrupt();


        });


                trio.start();

        new Thread() {
            @Override
            public void run() {
                while (del==false) {

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                         buttonTenMin.getScene().getWindow().hide();
                    }
                });



            }
        }.start();

    }

   public void initData(ITask task) {
        this.tempTask = task;
        textAreaName.setText(tempTask.getName());
        textAreaDescription.setText(tempTask.getDescription());
        int flagig = tempTask.getOtlozhit();

        if(flagig==0){
            labelInformation.setText("До задачи осталось 15 минут");
        }

       if(flagig==1){
           labelInformation.setText("До задачи осталось 10 минут");
           buttonTenMin.setDisable(true);
       }
       if(flagig==2){
           labelInformation.setText("До задачи осталось 5 минут");
           buttonTenMin.setDisable(true);
       }
    }
}
