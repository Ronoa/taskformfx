package sample;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import sample.client.ClientLoader;
import sample.client.PacketIspolnitelei;
import sample.taskwork.Contact;
import sample.taskwork.IContact;
import sample.taskwork.TaskWork;

public class AddContactController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField textNumberContact;

    @FXML
    private TextField textNameContact;

    @FXML
    private Button addContactToListButtom;

    @FXML
    private Label labelError;

    @FXML
    void initialize() {
        //labelError.setVisible(false);
        labelError.setText(" ");
        addContactToListButtom.setOnAction(event -> {

            if(textNameContact.getText().equals("") || textNumberContact.getText().equals("")){
                //labelError.setVisible(true);
                labelError.setText("Заполните все поля!");
            }
            else {
                if(TaskWork.getCheck( textNumberContact.getText()))
                    labelError.setText("Номер уже используется!");
                else{
                    String name,numb;
                    name=textNameContact.getText();
                    numb= textNumberContact.getText();

                    addTask(new Contact(name,numb));
                    addContactToListButtom.getScene().getWindow().hide();
                    ClientLoader.sendPacket(new PacketIspolnitelei(name,numb));
                }

            }
        });
    }
    public   void addTask(IContact contact) {
        TaskWork.addContactIsp(contact);

    }
}