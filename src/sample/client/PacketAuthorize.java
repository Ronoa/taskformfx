package sample.client;

import sample.AuthController;

import java.io.*;

public class PacketAuthorize extends OPacket {

    private String nickname;
    private String pass;
    private boolean flag =false;


    public PacketAuthorize(){

    }

    public PacketAuthorize(String nickname,String pass, Boolean flag){
        this.nickname=nickname;
        this.pass=pass;
        this.flag= flag;
    }

    @Override
    public short getId() {
        return 1;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        dos.writeUTF(nickname);
        dos.writeUTF(pass);
    }

    @Override
    public void read(DataInputStream dis) throws IOException {
        flag= dis.readBoolean();
    }
    @Override
    public void handle()  {
        System.out.println(String.format("nickname[%s] pass[%s] flag[%s] ",nickname,pass,flag));

        if(flag ==true){
            AuthController.flag1=1;
        } else AuthController.flag1=2;

    }
}
