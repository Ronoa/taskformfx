package sample.client;

import sample.taskwork.Contact;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketIspolnitelei extends OPacket {
   private String name,numb;

    PacketIspolnitelei(){

    }

    public PacketIspolnitelei(String name, String numb) {
        this.name = name;
        this.numb = numb;
    }

    @Override
    public short getId() {
        return 4;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
            dos.writeUTF(name);
            dos.writeUTF(numb);
    }

    @Override
    public void read(DataInputStream dis) throws IOException {


    }

    @Override
    public void handle() {
        System.out.println("На сервер отправлен новый исполнитель: "+name +" "+numb);

    }
}
