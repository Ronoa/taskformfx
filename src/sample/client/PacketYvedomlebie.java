package sample.client;

import javafx.application.Platform;
import sample.Controller;
import sample.taskwork.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class PacketYvedomlebie extends OPacket{
    private ITask task;

    public PacketYvedomlebie() {
    }

    public PacketYvedomlebie(ITask task) {
        this.task = task;
    }

    @Override
    public short getId() {
        return 7;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {

    }

    @Override
    public void read(DataInputStream dis) throws IOException {
        String name=dis.readUTF();

        String des = dis.readUTF();

        Long lg=dis.readLong();

        int countIs= dis.readInt();

        ICList clist=new ContactList();
        for(int i=0;i<countIs;i++){
            String name1=dis.readUTF();
            String number1 = dis.readUTF();
            clist.addContactEnd(new Contact(name1,number1));
        }
        Date data = new Date(lg);
        Calendar cal =  Calendar.getInstance();
        cal.setTime(data);
        ITask task2=new Task(name,des,cal,clist);
        TaskWork.removeTask2(task2);
        TaskWork.flagYved=true;
        Platform.runLater(() -> {

                                String str=task2.getName();
                                TaskWork.trayIcon.displayMessage(
                                        "Выполненная задача:",
                                        str,
                                        java.awt.TrayIcon.MessageType.INFO
                                );
                            });
    }

    @Override
    public void handle() {

    }
}
