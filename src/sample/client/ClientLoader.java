package sample.client;

import javafx.scene.control.Alert;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ClientLoader extends Thread {

    private static Socket socket;
    private  static boolean sentNickname =false;


    public ClientLoader(){

    }

    @Override
    public void run(){
        connect();
        handle();

       // end();
    }

//    public static void main(String[] args) {
//        connect();
//        handle();
//
//        end();
//    }

    private  static  void  connect(){
        try{
            socket = new Socket("localhost",8888);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    private static void readChat(){
//        Scanner scan =new Scanner(System.in);
//        while (true){
//
//            if(scan.hasNextLine()){
//                String line = scan.nextLine();
//                if(line.equals("/end"))
//                    end();
//                if(!sentNickname){
//                    sentNickname=true;
//                    sendPacket(new PacketAuthorize(line));
//                    continue;
//                }
//                sendPacket(new PacketMassage(null,line));
//            }else{
//                try{
//                    Thread.sleep(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//    }

    private static void handle(){
        Thread handler = new Thread(){

            @Override
            public void run(){
                while (true){
                    try {
                        DataInputStream dis = new DataInputStream(socket.getInputStream());
                        if(dis.available() <=0){
                            try{
                                Thread.sleep(10);
                            } catch (InterruptedException e) {

                            }
                            continue;
                        }

                        short id = dis.readShort();
                        OPacket packet = PacketManager.getPacket(id);
                        packet.read(dis);
                        packet.handle();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try{
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        };
        handler.start();
       // readChat();
    }

    private  static  void  end(){
        try{
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
    public   static void sendPacket(OPacket packet){
        try{
            DataOutputStream dos=new DataOutputStream(socket.getOutputStream());
            dos.writeShort(packet.getId());
            packet.write(dos);
            dos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
