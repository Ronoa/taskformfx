package sample.client;

import sample.taskwork.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class PacketTaskWork extends OPacket{
    private ITList task;
    private ICList isp;


    public PacketTaskWork(){}
    public PacketTaskWork(ITList task, ICList isp){
        this.task= task;
        this.isp= isp;

    }

    @Override
    public short getId() {
        return 3;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {

        ///write вроде лишний тут
        int countTask=task.getSize();
        int countIsp=isp.getSize();

        //пишем таски
        dos.writeInt(countTask);
        for(int i=0;i<countTask;i++){
            dos.writeUTF(task.getTaskToIndex(i).getName());
            dos.writeUTF(task.getTaskToIndex(i).getDescription());
            dos.writeLong(task.getTaskToIndex(i).getDate().getTime().getTime());

            dos.writeBoolean(task.getTaskToIndex(i).isYvedomlenie());
            dos.writeInt(task.getTaskToIndex(i).getOtlozhit());

            int ty= task.getTaskToIndex(i).getContactList().getSize();
            dos.writeInt(ty);
            for(int j=0;j<ty;j++){
                dos.writeUTF( task.getTaskToIndex(i).getContactList().getContactByIndex(j).getName());
                dos.writeUTF(task.getTaskToIndex(i).getContactList().getContactByIndex(j).getNumber());
            }

        }
        //пишем исполнителей
        dos.writeInt(countIsp);
        for(int i=0;i<countIsp;i++){
            dos.writeUTF(isp.getContactByIndex(i).getName());
            dos.writeUTF(isp.getContactByIndex(i).getNumber());
        }
    }

    @Override
    public void read(DataInputStream dis) throws IOException {
            int countTask = dis.readInt();
            ITList tt=new TaskList();
            for(int i=0;i<countTask;i++){
                String name = dis.readUTF();
                String des= dis.readUTF();
                Long lg=dis.readLong();

                boolean flag = dis.readBoolean();
                int t=dis.readInt();

                int y = dis.readInt();
                ICList tc= new ContactList();
                for(int j=0;j<y;j++){
                    String nname = dis.readUTF();
                    String nnumber = dis.readUTF();
                    tc.addContactEnd(new Contact(nname,nnumber));
                }
                Date data = new Date(lg);
                Calendar cal =  Calendar.getInstance();
                cal.setTime(data);
                tt.addTaskEnd(new Task(name,des,cal,tc,flag,t));
            }


            int countIsp = dis.readInt();
            ICList tc=new ContactList();
            for(int i=0;i<countIsp;i++){
                String name=dis.readUTF();
                String number = dis.readUTF();
                tc.addContactEnd(new Contact(name,number));
            }

            TaskWork.setMyListIsp(tc);
            TaskWork.setMyTaskList(tt);

        TaskWork.flagYved=true;
    }

    @Override
    public void handle() {

    }
}
