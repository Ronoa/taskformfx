package sample.client;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.InfoController;
import sample.taskwork.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class PacketPeresilka extends OPacket {
    private ITask task;
    public PacketPeresilka(){}
    public PacketPeresilka(ITask task) {
        this.task = task;
    }
    @Override
    public short getId() {
        return 8;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        dos.writeUTF(task.getName());
        dos.writeUTF(task.getDescription());
        dos.writeLong(task.getDate().getTime().getTime());

        int countIs= task.getContactList().getSize();
        dos.writeInt(countIs);

        for(int i=0;i<countIs;i++){
            dos.writeUTF(task.getContactList().getContactByIndex(i).getName());
            dos.writeUTF(task.getContactList().getContactByIndex(i).getNumber());
        }
        dos.writeBoolean(task.isYvedomlenie());
        dos.writeInt(task.getOtlozhit());
        System.out.println("PacketPeresilka (write) name:"+ task.getName() +" "
                +"flag:" +task.getOtlozhit() +" "
                +"st:" +task.isYvedomlenie());


    }

    @Override
    public void read(DataInputStream dis) throws IOException {

        String name=dis.readUTF();

        String des = dis.readUTF();

        Long lg=dis.readLong();

        boolean flag=dis.readBoolean();
        int t=dis.readInt();

        int countIs= dis.readInt();

        ICList clist=new ContactList();
        for(int i=0;i<countIs;i++){
            String name1=dis.readUTF();
            String number1 = dis.readUTF();
            clist.addContactEnd(new Contact(name1,number1));
        }
        Date data = new Date(lg);
        Calendar cal =  Calendar.getInstance();
        cal.setTime(data);

        System.out.println("PacketPeresilka (read) name:"+ name +" "
                +"flag:" +t +" "
                +"st:" +flag);

        ITask task2=new Task(name,des,cal,clist,flag,t);
        this.task=task2;




    }

    @Override
    public void handle() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/sample/info.fxml"));
                try {
                    loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Parent root = loader.getRoot();
                Stage stage = new Stage();
                stage.setTitle("Task Manager");
                stage.setScene(new Scene(root, 370, 189));
                stage.setResizable(false);

                InfoController controller=loader.<InfoController>getController();
                controller.initData(task);
                stage.show();
            }
        });


    }
}
