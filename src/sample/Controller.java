package sample;

import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

import com.jfoenix.controls.*;
import javafx.application.Platform;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import sample.client.ClientLoader;
import sample.client.PacketRemoveTask;
import sample.taskwork.*;

import javax.swing.*;


public class Controller {

    private ObservableList<ITask> taskList = FXCollections.observableArrayList();

    private ObservableList<IContact> contactList = FXCollections.observableArrayList();

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button addTaskButtom;


    @FXML
    private TableView tableTask;

    @FXML
    private TableView tableIspolnitel;

    @FXML
    private TableColumn<Contact, String> columNameIsp;

    @FXML
    private TableColumn<Contact, String> columNameNumber;

    @FXML
    private TableColumn<Task, String> columName;

    @FXML
    private TableColumn<Task, String> columDescription;

    @FXML
    private TableColumn<Task, String> columDateTime;

    @FXML
    private TableColumn<Task, String> columContact;

    @FXML
    private Label countContact;

    @FXML
    private Button addContactButtom;

    @FXML
    private JFXButton savetask;

    @FXML
    private JFXButton importtask;

    @FXML
    private JFXTextField textName;

    @FXML
    private JFXTextArea textDescription;

    @FXML
    private JFXDatePicker calendar;

    @FXML
    private JFXTimePicker timepicker;


    final SystemTray tray = SystemTray.getSystemTray();
    @FXML
    private JFXButton buttomDel;

    @FXML
    void initialize() {
        update();
        //initContact();
        timepicker.setIs24HourView(true);


        //Thread mythread=new MyThread();
        // mythread.run();
        buttomDel.setOnAction(event -> {


            removeCell();
        });


        addContactButtom.setOnAction(event -> {
            createContactForm();
            countContact.setText(TaskWork.getContactListLength().toString());
            updateIsp();
//            java.awt.Image image = null;
//            try {
//                image = ImageIO.read(new File("./icon.png"));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            java.awt.TrayIcon trayIcon = new java.awt.TrayIcon(image);
//            try {
//                tray.add(trayIcon);
//            } catch (AWTException e) {
//                e.printStackTrace();
//            }
//           TaskWork.trayIcon.displayMessage(
//                    "Выполненно",
//                    "The time is now ",
//                    java.awt.TrayIcon.MessageType.INFO
//            );
//            tray.remove(trayIcon);


        });


        addTaskButtom.setOnAction(event -> {
            if (textName.getText().equals("") || textDescription.getText().equals("")) {
                //ошибка полей
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Ошибка");
                alert.setHeaderText("Ошибка при добавлении задачи");
                if (textName.getText().equals(""))
                    alert.setContentText("Заполните поле <Название>");
                else alert.setContentText("Заполните поле <Описание>");
                alert.show();

            } else {
                //поля заполнены
                if (TaskWork.getContactListLength() == 0) {
                    //ошибка контакта
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Ошибка");
                    alert.setHeaderText("Ошибка при добавлении задачи");
                    alert.setContentText("Нет контактов");
                    alert.show();
                } else {
                    Date date2 = getDate().getTime();
                    long ctime = getDateDiff(date2, TimeUnit.MINUTES);
                    int tp = 0;
                    if (ctime > 10 && ctime <= 15) tp = 1;
                        else if (ctime > 5 && ctime <= 10) tp = 2;
                            else if (ctime > 0 && ctime <= 5) tp = 3;
                    TaskWork.addTaskToList(textName.getText(), textDescription.getText(), getDate(), true, tp);
                    System.out.println("Создана задача имя:" +textName.getText()+ " flag:"+ tp +" ||| ctime-"+ctime);
                    countContact.setText(TaskWork.getContactListLength().toString());
                    clear();
                    update();
                    clear2();
                }
            }
        });
        savetask.setOnAction(event -> {
            new Thread() {
                @Override
                public void run() {
                    JFileChooser chooser = new JFileChooser();
                    chooser.setFileSelectionMode(chooser.DIRECTORIES_ONLY);


                    Platform.runLater(() -> {

                        TaskWork.stage.hide();
                    });


                    int result = chooser.showOpenDialog(null);
                    if (result == JFileChooser.APPROVE_OPTION) {
                        File selectedFile = chooser.getSelectedFile();
                        String path = selectedFile.getAbsolutePath();

                        FileOutputStream fis = null;
                        try {
                            fis = new FileOutputStream(path + "/" + TaskWork.nicname + ".file");
                            ObjectOutputStream ois = new ObjectOutputStream(fis);
                            ois.writeObject(TaskWork.getCurrentTaskList());
                            //ois.writeObject(TaskWork.getCurrentRemoveTaskList());
                            //ois.writeObject(TaskWork.getCurrentContactList());
                            ois.writeObject(TaskWork.getCurrentListIsp());
                            ois.flush();
                            ois.close();
                            fis.flush();
                            fis.close();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Platform.runLater(() -> {

                            TaskWork.stage.show();
                        });
                    }
                    //if the user click on save in Jfilechooser


                    else if (result == JFileChooser.CANCEL_OPTION) {
                        System.out.println("No File Select");
                    }
                }
            }.start();


//            serialaizeble();
        });
        importtask.setOnAction(event -> {

            //deserialaizeble();
        });

//        new Thread(() -> {
//            while (true) {
//                try {
//                    Thread.sleep(10000);
//                    Date date = new Date();
//                    int num = TaskWork.getSize2();
//                    ITList removeList = new TaskList();
//                    ITList tempList = TaskWork.getCurrentTaskList();
//                    if (num > 0) {
//                        for (int i = 0; i < num; i++) {
//                            Date dateTask = tempList.getTaskToIndex(i).getDate().getTime();
//                            if (date.compareTo(dateTask) >= 0) {
//                                removeList.addTaskEnd(tempList.getTaskToIndex(i));
//                            }
//                        }
//
//                        int numRem = removeList.getSize();
//                        if (numRem > 0) {
//                            TaskWork.setMyRemoveTaskList(removeList);
//
//
//                            for (int j = 0; j < numRem; j++)
//                                TaskWork.removeTask(removeList.getTaskToIndex(j));
//
//                            Platform.runLater(() -> {
//                                update();
//                                //createInfoForm();
//                                String str="";
//                                ITList remove=TaskWork.getRemoveTaskList();
//                                for(int i=0;i<remove.getSize();i++)
//                                {
//                                    str+=remove.getTaskToIndex(i).getName() + " |";
//                                }
//                                str=str.substring(0,str.length()-2);
//                                TaskWork.trayIcon.displayMessage(
//                                        "Выполненные задачи:",
//                                        str,
//                                        java.awt.TrayIcon.MessageType.INFO
//                                );
//                            });
//                        }
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();

        new Thread() {
            @Override
            public void run() {
                while (true) {
                    if (TaskWork.flagYved) {
                        TaskWork.flagYved = false;
                        System.out.println("Update Table");
                        update3();
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }


            }
        }.start();
    }

    public Calendar getDate() {

        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, this.calendar.getValue().getYear());
        calendar.set(Calendar.MONTH, this.calendar.getValue().getMonthValue() - 1);
        calendar.set(Calendar.DAY_OF_MONTH, this.calendar.getValue().getDayOfMonth());
        calendar.set(Calendar.HOUR_OF_DAY, timepicker.getValue().getHour());
        calendar.set(Calendar.MINUTE, timepicker.getValue().getMinute());
        calendar.set(Calendar.SECOND, 0);

        return calendar;
    }

    public void createContactForm() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/addcontact.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root, 330, 330));
        stage.setResizable(false);
        stage.showAndWait();
    }

    public void createSeeContactForm() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/seecontact.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setTitle("Task Manager");
        stage.setScene(new Scene(root, 330, 330));
        stage.setResizable(false);
        stage.showAndWait();
    }

    public void createInfoForm() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/info.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root, 370, 180));
        stage.setResizable(false);
        stage.showAndWait();
    }

    public void clear() {
        textName.clear();
        textDescription.clear();
        timepicker.setValue(null);
        calendar.setValue(null);
    }

    public void clear2() {
        tableIspolnitel.getItems().removeAll(contactList);
        TaskWork.getTableColum2(contactList);

        tableIspolnitel.setItems(contactList);
    }

    public void update() {

        tableTask.getItems().removeAll(taskList);
        TaskWork.getTable(taskList);


        tableTask.setItems(taskList);
//        columName.setCellValueFactory(new PropertyValueFactory<Task, String>("name"));
//        columDescription.setCellValueFactory(new PropertyValueFactory<Task, String>("description"));
        columName.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getName()+" flag:"+ cellData.getValue().getOtlozhit()+" y:"+ cellData.getValue().isYvedomlenie()));
        columDescription.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getDescription()));

        columDateTime.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getDateString()));
        columContact.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getContactList().getAllContactToString()));

    }

    public void update3() {
        tableTask.getItems().clear();
        tableTask.refresh();
        TaskWork.getTable(taskList);
        tableTask.setItems(taskList);

    }


    public void updateIsp() {

        tableIspolnitel.getItems().removeAll(contactList);
        TaskWork.getTableColum2(contactList);

        tableIspolnitel.setItems(contactList);
        columNameIsp.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getName()));
        columNameNumber.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getNumber()));

    }


    public void getCell() {

        ITask tt = (ITask) tableTask.getSelectionModel().getSelectedItem();
    }

    public void removeCell() {
        ITask tt = (ITask) tableTask.getSelectionModel().getSelectedItem();
        tableTask.getItems().removeAll(tt);
        TaskWork.removeTask(tt);
        ClientLoader.sendPacket(new PacketRemoveTask(tt));
        //tableTask. tableTask.getSelectionModel().getSelectedCells().remove();
    }

    public void initContact() {

        TaskWork.addContactIsp(new Contact("Владимир", "0001"));
        TaskWork.addContactIsp(new Contact("Петр", "0002"));
        TaskWork.addContactIsp(new Contact("Гриша", "0003"));
        TaskWork.addContactIsp(new Contact("Федор", "0004"));
        TaskWork.addContactIsp(new Contact("Саша", "0005"));

    }

    /*
        public class MyThread extends Thread {
            public void run() {

                while (true){
                    try {
                        Thread.sleep(1000);
                        Date date= new Date();
                        int num=TaskWork.getSize();
                        ITList removeList=new TaskList();
                        ITList tempList=TaskWork.getCurrentTaskList();
                        if(num>0){
                            for(int i=0;i<num;i++)
                            {
                                Date dateTask =tempList.getTaskToIndex(i).getDate().getTime();
                                if(dateTask.compareTo(date)>=0){
                                    removeList.addTaskEnd(tempList.getTaskToIndex(i));
                                }
                            }
                        }
                        int numRem=removeList.getSize();
                        for(int j=0;j<numRem;j++)
                            TaskWork.removeTask(removeList.getTaskToIndex(j));
                        TaskWork.setMyRemoveTaskList(removeList);
                        createInfoForm();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    */
    public void serialaizeble() {


        FileOutputStream fis = null;
        try {
            fis = new FileOutputStream("./" + TaskWork.nicname + ".file");
            ObjectOutputStream ois = new ObjectOutputStream(fis);
            ois.writeObject(TaskWork.getCurrentTaskList());
            //ois.writeObject(TaskWork.getCurrentRemoveTaskList());
            //ois.writeObject(TaskWork.getCurrentContactList());
            ois.writeObject(TaskWork.getCurrentListIsp());
            ois.flush();
            ois.close();
            fis.flush();
            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String pickPath(JFileChooser fileChooser) {
        String path = null;

        /* create a JFrame to be the parent of the file
         * chooser open dialog if you don't do this then
         * you may not see the dialog.
         */
        JFrame frame = new JFrame();
        frame.setAlwaysOnTop(true);

        // get the return value from choosing a file
        int returnVal = fileChooser.showOpenDialog(frame);

        // if the return value says the user picked a file
        if (returnVal == JFileChooser.APPROVE_OPTION)
            path = fileChooser.getSelectedFile().getPath();
        return path;

    }

    public void deserialaizeble() {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("./task.file");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ITList desTask = (ITList) ois.readObject();
            //ITList desRemove= (ITList) ois.readObject();
            //ICList desContact= (ICList) ois.readObject();
            ICList desIsp = (ICList) ois.readObject();
            ois.close();
            fis.close();

            clear();


            tableTask.getItems().removeAll(taskList);

            TaskWork.setMyTaskList(desTask);
            TaskWork.setMyListIsp(desIsp);

            TaskWork.getTable(taskList);

            tableTask.setItems(taskList);
            //columName.setCellValueFactory(new PropertyValueFactory<Task, String>("name"));
            //columDescription.setCellValueFactory(new PropertyValueFactory<Task, String>("description"));

            columName.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getName()+" flag:"+ cellData.getValue().getOtlozhit()+" y:"+ cellData.getValue().isYvedomlenie()));
            columDescription.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getDescription()));
            columDateTime.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getDateString()));
            columContact.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getContactList().getAllContactToString()));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public long getDateDiff(Date date1, TimeUnit timeUnit) {
        Date data = new Date();
        long diffInMillies = date1.getTime() - data.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }


}
