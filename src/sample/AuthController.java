package sample;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import sample.client.ClientLoader;
import sample.client.PacketAuthorize;
import sample.client.PacketMassage;
import sample.taskwork.*;

public class AuthController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;


    @FXML
    private JFXTextField loginText;

    @FXML
    private JFXPasswordField passwordText;

    @FXML
    private JFXButton buttomAuth;

    @FXML
    private  Label labelError;
    public   static volatile int flag1  =0;

    private Thread tr;
    @FXML
    void initialize() {

        buttomAuth.setOnAction(event -> {


            ClientLoader.sendPacket(new PacketAuthorize(loginText.getText(),passwordText.getText(),false));
            for(int i=0;i<10;i++){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (flag1!=0)  {
                    if (flag1==1) {
                        flag1=0;
                        Stage stage = (Stage) buttomAuth.getScene().getWindow();
                        stage.close();
                        TaskWork.nicname=loginText.getText();
                        i=100;
                        createForm();
                        //Thread.currentThread().interrupt();

                        break;

                    }
                    if (flag1==2) {
                        flag1=0;
                        labelError.setText("Неверный логин/пароль!");
                    }

                }
            }


        });


//        while (true){
//                    if (flag1==0) continue;
//                    if (flag1==1) {
//                        flag1=0;
//                        Stage stage = (Stage) buttomAuth.getScene().getWindow();
//                        stage.close();
//                        createForm();
//                        Thread.currentThread().interrupt();
//                        break;
//                    }
//                    if (flag1==2) {
//                        flag1=0;
//                        labelError.setText("Неверный логин/пароль!");
//                        continue;
//                    }
//                    try {
//                        Thread.sleep(10);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//        tr= new Thread(){
//            @Override
//            public void run() {
//                while (true){
//                    if (flag1==0) continue;
//                    if (flag1==1) {
//                        flag1=0;
//                        Stage stage = (Stage) buttomAuth.getScene().getWindow();
//                        stage.close();
//                        createForm();
//                        tr.interrupt();
//                        break;
//                    }
//                    if (flag1==2) {
//                        flag1=0;
//                        labelError.setText("Неверный логин/пароль!");
//                        continue;
//                    }
//                    try {
//                        Thread.sleep(10);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        };
//        tr.start();

    }

    public  void neprav(){
        labelError.setText("Неверный логин/пароль!");
    }

    public void createForm() {


        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/sample.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
//        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
//                  public void handle(WindowEvent we) {
//                      System.out.println("Stage is closing");
//                      System.exit(0);
//                  }
//              });
        stage.setTitle("Task Manager");
        stage.setScene(new Scene(root, 690, 510));
        stage.setResizable(false);
        TaskWork.stage=stage;
        stage.show();

    }

}
