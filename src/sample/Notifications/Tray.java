package sample.Notifications;

import sample.taskwork.Task;

import java.awt.*;

public class Tray {
    private TrayIcon trayIcon;
    private SystemTray tray;
    private Image image;
    public Tray() throws AWTException{
        tray = SystemTray.getSystemTray();
        image = Toolkit.getDefaultToolkit().createImage("icon.png");
        trayIcon = new TrayIcon(image, "Tray Demo");
        trayIcon.setImageAutoSize(true);
        trayIcon.setToolTip("System tray icon demo");
        tray.add(trayIcon);
    }
    public Tray(String toolTip, String icon) throws AWTException{
        tray = SystemTray.getSystemTray();
        image = Toolkit.getDefaultToolkit().createImage(icon);
        trayIcon = new TrayIcon(image, toolTip);
        trayIcon.setImageAutoSize(true);
       // trayIcon.setToolTip();
        tray.add(trayIcon);
    }
    public void displayTray(Task task){
        trayIcon.displayMessage(task.getName(), task.getDescription(), TrayIcon.MessageType.INFO);
    }
}
