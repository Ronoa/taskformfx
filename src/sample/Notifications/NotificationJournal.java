package sample.Notifications;

import sample.taskwork.TaskList;

public class NotificationJournal {
    private NotificationTask[] taskList;
    Tray tray;
    public NotificationJournal(TaskList taskList, Tray tray){
        this.tray = tray;
        this.taskList = new NotificationTask[taskList.getSize()];
        for (int i = 0; i < taskList.getSize(); i++) {
            this.taskList[i] = new NotificationTask(taskList.getTaskToIndex(i));
        }
    }

}
