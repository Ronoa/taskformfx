package sample.taskwork;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class Task  implements ITask, Serializable {
    private String name;
    private String description;
    private Calendar date;
    private ICList contactList;


    private boolean yvedomlenie=true;//(после первого всплытия за 15 мин)//true - уведомлять
                                        //false - не уведомлять
    private int otlozhit=0;//0 - не отложивать
                            //1- отложить на 5 мин
                            //2- отложить на 10 мин

//    public Task(){
//        name=null;
//        description=null;
//        date=null;
//        contactList=null;
//    }

    public Task(String name,String description, Calendar date, ICList contactList){
        this.name=name;
        this.description=description;
        this.date=date;
        this.contactList=contactList;

    }
    public Task(String name,String description, Calendar date, ICList contactList, int flagNum){
        this.name=name;
        this.description=description;
        this.date=date;
        this.contactList=contactList;
        this.otlozhit=flagNum;
    }
    public Task(String name,String description, Calendar date, ICList contactList,boolean yved, int flagNum){
        this.name=name;
        this.description=description;
        this.date=date;
        this.contactList=contactList;
        this.yvedomlenie=yved;
        this.otlozhit=flagNum;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public Calendar getDate() {

        return date;
    }
    public  String getDateString(){
        StringBuffer sb = new StringBuffer();
        sb.append("Дата ");
        sb.append(date.get(Calendar.DAY_OF_MONTH) + ".");
        int d=date.get(Calendar.MONTH) +1;
        String se=String.format("%02d" ,d );
        sb.append( se + ".");
        sb.append(date.get(Calendar.YEAR) + " Время ");
        sb.append(date.get(Calendar.HOUR_OF_DAY) + ":");
        sb.append(date.get(Calendar.MINUTE) );

        return  sb.toString();
    }

    public void setDate(Calendar date) {

        this.date = date;
    }

    public ICList getContactList() {

        return contactList;
    }
    public boolean isYvedomlenie() {
        return yvedomlenie;
    }

    public void setYvedomlenie(boolean yvedomlenie) {
        this.yvedomlenie = yvedomlenie;
    }

    public int getOtlozhit() {
        return otlozhit;
    }

    public void setOtlozhit(int otlozhit) {
        this.otlozhit = otlozhit;
    }


    public  String getAllContactListString(){
        return contactList.toString();
    }

    public void setContactList(ICList contactList) {
        this.contactList = contactList;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("<");
        sb.append(name);
        sb.append(",");
        sb.append(description);
        sb.append(",");
//        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        sb.append(date.get(Calendar.DAY_OF_MONTH) + " ");
        sb.append(date.get(Calendar.MONTH) + " ");
        sb.append(date.get(Calendar.YEAR) + " ");
        sb.append(date.get(Calendar.HOUR) + ":");
        sb.append(date.get(Calendar.MINUTE) );
        sb.append(",");
        sb.append(contactList.toString());
        sb.append(">");

        return sb.toString();
    }
}
