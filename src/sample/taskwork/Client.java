package sample.taskwork;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.PublicKey;

public class Client {

    public static Socket socket;
    public static ObjectInputStream input;
    public static ObjectOutputStream output;


    public  static void handle(){
        Thread handler = new Thread(){
            @Override
            public void run() {
                while (true){
                    try{
                        input = new ObjectInputStream(socket.getInputStream());

                        // = new ObjectOutputStream(socket.getOutputStream());
                        if(input.available() <=0){
                            Thread.sleep(10);
                            continue;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        handler.start();
    }


    public static void connect() {
        try {

            socket = new Socket("localhost", 8888);
            //input = new ObjectInputStream(socket.getInputStream());
            //output = new ObjectOutputStream(socket.getOutputStream());


        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void close(){
        try{
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static  void sendAndAccept(Object obj){
        try{
            input = new ObjectInputStream(socket.getInputStream());
            output = new ObjectOutputStream(socket.getOutputStream());
            output.writeObject(obj);
            output.flush();
            System.out.println((String )input.readObject());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

}
