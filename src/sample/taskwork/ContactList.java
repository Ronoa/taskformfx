package sample.taskwork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ContactList implements ICList, Serializable {

    private List<IContact> contactList;

    {
        contactList=new ArrayList<>();
    }

    public ContactList(){
    }
    public ContactList(IContact ...contacts){

        for (IContact contact:contacts) {
            contactList.add(contact);

        }
    }

    public  ContactList(IContact c1){
        contactList.add(c1);
    }
    public  ContactList(IContact c1,IContact c2){
        contactList.add(c1);
        contactList.add(c2);
    }

    public void setContactList(List<IContact> contactList){
        this.contactList = contactList;
    }
    public void setContact(int index,IContact contact) throws IndexOutOfBoundsException {
        contactList.set(index,contact);
    }
    public int getSize(){
        return  contactList.size();
    }
    public IContact getContactByIndex(int i){
        return contactList.get(i);
    }
    public List<IContact> getContactList() {
        return contactList;
    }
    public void  addContactEnd(IContact contact){
        contactList.add(contact);
    }

    public void  addContactIndex(int value,IContact contact) throws IndexOutOfBoundsException{
        contactList.add(value,contact);
    }
    public  int getLength(){
        return contactList.size();
    }

    public  String getAllContactToString(){

            StringBuffer sb = new StringBuffer();
            //sb.append("[");
            for (IContact val:contactList) {
                sb.append(val.getContactToString());
                sb.append(",");
            }
            sb.deleteCharAt(sb.length()-1);
            // sb.append("]");

            return sb.toString();
        }
    @Override
    public String toSting()
    {
        StringBuffer sb = new StringBuffer();
        //sb.append("[");
        for (IContact val:contactList) {
            sb.append(val.toString());
            sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);
       // sb.append("]");

        return sb.toString();
    }
}
