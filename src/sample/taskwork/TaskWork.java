package sample.taskwork;

import javafx.collections.ObservableList;
import javafx.stage.Stage;
import sample.client.ClientLoader;
import sample.client.PacketNewTask;

import javax.imageio.ImageIO;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class TaskWork implements Serializable {
    private static ITList myTaskList = new TaskList();
    private static ITList myRemoveTaskList = new TaskList();
    private static ICList myContactList = new ContactList();
    private static ICList myListIsp = new ContactList();
    public static java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
    public static java.awt.TrayIcon trayIcon;
    public static Stage stage;
    public static String nicname;
    public static boolean flagYved = false;


    static {
        try {
            trayIcon = new java.awt.TrayIcon(ImageIO.read(new File("./icon.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setMyTaskList(ITList list) {
        myTaskList = list;
    }

    public static void setMyRemoveTaskList(ITList list) {
        myRemoveTaskList = list;
    }

    public static void setMyContactList(ICList list) {
        myContactList = list;
    }

    public static void setMyListIsp(ICList list) {
        myListIsp = list;
    }

    ///////
    public static ITList getCurrentTaskList() {
        return myTaskList;
    }

    public static ITList getCurrentRemoveTaskList() {
        return myRemoveTaskList;
    }

    public static ICList getCurrentContactList() {
        return myContactList;
    }

    public static ICList getCurrentListIsp() {
        return myListIsp;
    }
    //////


    public static ITList getRemoveTaskList() {
        return myRemoveTaskList;
    }

    public static int getSize() {
        return myListIsp.getSize();
    }

    ;

    public static int getSize2() {
        return myTaskList.getSize();
    }

    ;

    public static void addContactToList(IContact contact) {
        myContactList.addContactEnd(contact);
    }

    public static void addContactIsp(IContact contact) {
        myListIsp.addContactEnd(contact);
    }


    public static void addTaskToList(String name, String description, Calendar date,boolean y,int flag) {
        ITask task = new Task(name, description, date, myContactList,y,flag);
        myTaskList.addTaskEnd(task);
        ClientLoader.sendPacket(new PacketNewTask(task));
        myContactList = new ContactList();
    }

    public static Integer getContactListLength() {
        return myContactList.getLength();
    }

    public static void getTable(ObservableList<ITask> observableList) {
        observableList.removeAll();
        int i = myTaskList.getSize();
        int j = 0;
        while (j < i) {
            observableList.add(myTaskList.getTaskToIndex(j));
            j++;
        }
    }

    public static void getTableColum(ObservableList<IContact> observableList) {
        observableList.removeAll();
        int i = myListIsp.getSize();
        int j = 0;
        while (j < i) {
            observableList.add(myListIsp.getContactByIndex(j));
            j++;
        }
    }

    public static void getTableColum2(ObservableList<IContact> observableList) {
        observableList.removeAll();
        int i = myContactList.getSize();
        int j = 0;
        while (j < i) {
            observableList.add(myContactList.getContactByIndex(j));
            j++;
        }
    }

    public static boolean getCheck(String str) {
        boolean flag = false;

        int i = myListIsp.getSize();
        int j = 0;
        while (j < i) {
            if (myListIsp.getContactByIndex(j).getNumber().equals(str))
                flag = true;

            j++;
        }

        return flag;
    }

    public static ObservableList<ITList> getTaskData() {
        return (ObservableList<ITList>) myTaskList;
    }

    public static void removeTask(ITask task) {
        myTaskList.removeTask(task);
    }

    public static void removeTask2(ITask task) {
        ITask temptask = null;
        boolean t = false;
        Long lg = task.getDate().getTime().getTime();
        int i = myTaskList.getSize();

        for (int j = 0; j < i; j++) {
            if (myTaskList.getTaskToIndex(j).getDate().getTime().getTime() == lg) {
                if (myTaskList.getTaskToIndex(j).getName().equals(task.getName()) && myTaskList.getTaskToIndex(j).getDescription().equals(task.getDescription())
                        && (myTaskList.getTaskToIndex(j).getContactList().getSize() == task.getContactList().getSize())) {

                    int yu = myTaskList.getTaskToIndex(j).getContactList().getSize();
                    for (int t1 = 0; t1 < yu; t1++) {
                        if (myTaskList.getTaskToIndex(j).getContactList().getContactByIndex(t1).getName().equals(task.getContactList().getContactByIndex(t1).getName()) &&
                                myTaskList.getTaskToIndex(j).getContactList().getContactByIndex(t1).getNumber().equals(task.getContactList().getContactByIndex(t1).getNumber())) {
                            t = true;

                        } else {
                            t = false;
                        }
                    }
                    if (t) {
                        temptask = myTaskList.getTaskToIndex(j);

                    }

                }
            }
        }

        if (temptask != null)
        {myTaskList.removeTask(temptask);
            TaskWork.flagYved=true;
        };

    }


}
