package sample.taskwork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TaskList implements  ITList, Serializable {
    private List<ITask> taskList;
    {
        taskList=new ArrayList<>();
    }
    public TaskList(){}
    public TaskList(ITask ...tasks){
        for (ITask task:tasks) {
            taskList.add(task);
        }
    }

    public void setTaskList(List<ITask> taskList) {
        this.taskList = taskList;
    }
    public void setTask(int index,ITask task) throws IndexOutOfBoundsException {
        taskList.set(index,task);
    }
    public  void removeTask(ITask task){
        taskList.remove(task);
    }
    public List<ITask> getTaskList() {
        return taskList;
    }
    public void  addTaskEnd(ITask task){
        taskList.add(task);
    }
    public int getSize(){
        return  taskList.size();
    }
    public void  addTaskIndex(int value,ITask task) throws IndexOutOfBoundsException{
        taskList.add(value,task);
    }
    public ITask getTaskToIndex(int i) throws  IndexOutOfBoundsException{
        return  taskList.get(i);
    }

    public String toSting()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("{");
        for (ITask val:taskList) {
            sb.append(val.toString());
            sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append("}");

        return sb.toString();
    }
}
