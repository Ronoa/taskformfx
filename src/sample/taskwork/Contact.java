package sample.taskwork;


import java.io.Serializable;

public class Contact implements  IContact, Serializable {
    private String name;
    private String number;

//    public  Contact(){
//        name=null;
//        number=null;
//    }

    public Contact(String name,String number){
        this.name=name;
        this.number=number;
    }

    public  void setName(String name){
        this.name=name;
    }

    public void setNumber(String number){
        this.number=number;
    }
    public String getName(){
        return name;
    }

    public String getNumber(){
        return  number;
    }

    public  String getContactToString(){
        StringBuffer sb = new StringBuffer();

        sb.append("(");
        sb.append(name);
        sb.append(",");
        sb.append(number);
        sb.append(")");

        return sb.toString();
    }
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        sb.append("(");
        sb.append(name);
        sb.append(",");
        sb.append(number);
        sb.append(")");

        return sb.toString();
    }
}
