package sample.taskwork;

import java.util.List;

public interface ITList {
    void setTaskList(List<ITask> taskList);
    void setTask(int index,ITask task);
    List<ITask> getTaskList();
    void  addTaskEnd(ITask task);
    void  addTaskIndex(int value,ITask task);
    int getSize();
    void removeTask(ITask task);
    ITask getTaskToIndex(int i);
    String toString();
}
