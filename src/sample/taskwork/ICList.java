package sample.taskwork;


import java.util.List;

public interface ICList  {
    void setContactList(List<IContact> contactList);
    void setContact(int index,IContact contact);
    List<IContact> getContactList();
    void  addContactEnd(IContact contact);
    void  addContactIndex(int value,IContact contact);
    int getLength();
    String getAllContactToString();
    int getSize();
    IContact getContactByIndex(int i);
    String toSting();
}
