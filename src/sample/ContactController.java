package sample;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.taskwork.Contact;
import sample.taskwork.IContact;
import sample.taskwork.Task;
import sample.taskwork.TaskWork;

public class ContactController {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button addContactToListButtom;

    @FXML
    private TableView tableContact;

    @FXML
    private TableColumn<Contact, String> nameColum;

    @FXML
    private TableColumn<Contact, String> numbColum;
    @FXML
    private Button addContIsp;
    private IContact contact;
    private ObservableList<IContact> contactList= FXCollections.observableArrayList();
    @FXML
    void initialize() {
        update();
        addContIsp.setOnAction(event -> {
            createContactForm();
            update();
        });

        addContactToListButtom.setOnAction(event -> {
                if(!tableContact.getSelectionModel().getSelectedItem().equals(null)){
                IContact tt= (IContact) tableContact.getSelectionModel().getSelectedItem();
                addTask(tt);
                addContactToListButtom.getScene().getWindow().hide();}

        });
    }
    public   void addTask(IContact contact) {
        TaskWork.addContactToList(contact);

    }

    public void update(){

        tableContact.getItems().removeAll(contactList);
        TaskWork.getTableColum(contactList);

        tableContact.setItems(contactList);
        nameColum.setCellValueFactory(cellData -> new SimpleObjectProperty<>( cellData.getValue().getName()));
        numbColum.setCellValueFactory(cellData -> new SimpleObjectProperty<>( cellData.getValue().getNumber()));

    }

    public  void  createContactForm(){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/addddcontact.fxml"));
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root,370, 170));
        stage.setResizable(false);
        stage.showAndWait();
    }
}